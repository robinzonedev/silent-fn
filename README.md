# silent-fn

Silents a function in case of error, and returns an optional default value instead (undefined by default)

Can be used as alternative to Lodash's get function, just instead of string or array it gets a function 
(and the 'path' part remains javascript)

# Installation
```bash
$ npm i --save silent-fn
```
# Usage
```js
import { silent } from 'silent-fn';

...

const getC = silent(x => x.a.b.c);
getC({a: 5}); // returns undefined instead of error
getC(undefined); // returns undefined instead of error
getC({}); // returns undefined instead of error
getC({a: {b: {c: 5}}}); // returns 5
```
