"use strict";

module.exports = function silent(f, toDefault) {
  return function () {
    try {
      return f.apply(void 0, arguments);
    } catch (e) {
      return toDefault;
    }
  };
};
